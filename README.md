## Libre Space Foundation KiCad Library

This repository contains the schematic, footprint and 3D libraries supported by the LSF KiCAD library team.

Most designed from scratch. Some 3D models are Hirose downloads, and [Smisioto's](http://smisioto.no-ip.org/elettronica/kicad/kicad-en.htm) released under CC-BY-SA v3

## License

Licensed under the [CERN OHLv1.2](LICENSE) or as referenced.
